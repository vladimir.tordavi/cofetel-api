import json
from pnn.models import Pnn
from pnn.serializers import PnnSerializer
from django.db import IntegrityError

def load_data():
    with open("pnn.json", "r", encoding="utf8") as data_file:
        data = json.load(data_file).get("pnn")

    not_created = []
        
    for data in data:

        try:

            id = data.get('ID')

            print(data)
            print(data.get("CLAVE_CENSAL"))
            print(data.get("ID"))
            print(data.get("NIR"))

            if not id:
                continue

            clave_censal = data.get("CLAVE_CENSAL")
            poblacion = data.get("POBLACION")
            municipio = data.get("MUNICIPIO")
            estado = data.get("ESTADO")
            presuscripcion = data.get("PRESUSCRIPCION")
            region = data.get("REGION")
            asl = data.get("ASL")
            nir = data.get("NIR")
            serie = data.get("SERIE")
            num_inicial = int(data.get("NUMERACION_INICIAL"))
            num_final = int(data.get("NUMERACION_FINAL"))
            ocupacion = data.get("OCUPACION")
            tipo_red = data.get("TIPO_RED")
            modalidad = data.get("MODALIDAD")
            razon_social = data.get("RAZON_SOCIAL")
            fecha_asignacion = data.get("FECHA_ASIGNACION")
            fecha_consolidacion = data.get("FECHA_CONSOLIDACION")
            fecha_migracion = data.get("FECHA_MIGRACION")
            nir_anterior = data.get("NIR_ANTERIOR")

            nir = str(nir)
            num_inicial = int(num_inicial)
            num_final = int(num_final)

            pnnSerializer = PnnSerializer(
                data = {
                    clave_censal:clave_censal,
                    poblacion:poblacion,
                    municipio:municipio,
                    estado:estado,
                    presuscripcion:presuscripcion,
                    region:region,
                    asl:asl,
                    nir:nir,
                    serie:serie,
                    num_inicial:num_inicial,
                    num_final:num_final,
                    ocupacion:ocupacion,
                    tipo_red:tipo_red,
                    modalidad:modalidad,
                    razon_social:razon_social,
                    fecha_asignacion:fecha_asignacion,
                    fecha_consolidacion:fecha_consolidacion,
                    fecha_migracion:fecha_migracion,
                    nir_anterior:nir_anterior
                }
            )

            if not pnnSerializer.is_valid():

                print(f"ERROR AT SERIALIZE: {pnnSerializer.errors}")
                return created

            pnn = Pnn.objects.create(
                clave_censal=clave_censal,
                poblacion=poblacion,
                municipio=municipio,
                estado=estado,
                presuscripcion=presuscripcion,
                region=region,
                asl=asl,
                nir=nir,
                serie=serie,
                num_inicial=num_inicial,
                num_final=num_final,
                ocupacion=ocupacion,
                tipo_red=tipo_red,
                modalidad=modalidad,
                razon_social=razon_social,
                fecha_asignacion=fecha_asignacion,
                fecha_consolidacion=fecha_consolidacion,
                fecha_migracion=fecha_migracion,
                nir_anterior=nir_anterior
            )

            if not pnn:

                print(f"ERROR AT CREATE: {clave_censal}")
                not_created.append(pnn)

        except TypeError as e:

            print(f"DATO INCORRRECTO: {e.args[0]}")
            not_created.append(pnn)

        except IntegrityError as e:

            print(f"DATO INVALIDO: {e.args[0]}")
            not_created.append(pnn)

        except ValueError as e:

            print(f"ERROR AL CREAR: {e.args[0]}")
            not_created.append(pnn)

    print(f"NOT CREATED: {len(not_created)}, DATA: {not_created}")
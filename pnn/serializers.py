from rest_framework import serializers
from pnn.models import Pnn


class PnnSerializer(serializers.ModelSerializer):

    class Meta:
        model = Pnn
        fields = '__all__'

    @staticmethod
    def get_created_date(obj):
        return obj.created.strftime("%d/%m/%Y")

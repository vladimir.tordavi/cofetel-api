from django.urls import path
from pnn import views

urlpatterns = [
    path('', views.PnnView.as_view()),
    path('validate/<str:phone>/', views.PhoneView.as_view()),
    path('data/', views.DataView.as_view()),
]
from django.db import models
from core.models import TimeStampedModel


# Create your models here.
class Pnn(TimeStampedModel):
    clave_censal = models.CharField(max_length=180, default="", blank=True)
    poblacion = models.CharField(max_length=180, default="", blank=True)
    municipio = models.CharField(max_length=180, default="", blank=True)
    estado = models.CharField(max_length=180, default="", blank=True)
    presuscripcion = models.CharField(max_length=180, default="", blank=True)
    region = models.CharField(max_length=180, default="", blank=True)
    asl = models.CharField(max_length=180, default="", blank=True)
    nir = models.CharField(max_length=180, blank=True)
    serie = models.CharField(max_length=180, blank=True)
    num_inicial = models.IntegerField(blank=True)
    num_final = models.IntegerField(blank=True)
    ocupacion = models.CharField(max_length=180, default="", blank=True)
    tipo_red = models.CharField(max_length=180, default="", blank=True)
    modalidad = models.CharField(max_length=180, default="", blank=True)
    razon_social = models.CharField(max_length=180, default="", blank=True)
    fecha_asignacion = models.CharField(max_length=180, default="", blank=True)
    fecha_consolidacion = models.CharField(max_length=180, default="", blank=True)
    fecha_migracion = models.CharField(max_length=180, default="", blank=True)
    nir_anterior = models.CharField(max_length=180, default="", blank=True)

    def __str__(self):
        return f'{self.nir} {self.serie} {self.num_inicial} - {self.num_final}'
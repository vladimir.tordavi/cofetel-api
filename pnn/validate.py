import json

from pnn.models import Pnn

def validate():
    with open("data.json", "r", encoding="utf8") as data_file:
        data = json.load(data_file).get("data")

    with open("validated.json", "r", encoding="utf8") as validated_data_file:
        validated_data = json.load(validated_data_file)
        
    validated_ids = []
    validated_customers = validated_data.get("customers")

    for customer in validated_customers:
        if customer.get('id') in validated_ids:
            continue
        validated_ids.append(customer.get('id'))
        
    for data in data:

        id = data.get('id')

        if not id:
            continue

        if id in validated_ids:
            continue
        
        phones = [
            data.get("phone_1") or "+52",
            data.get("phone_2") or "+52",
            data.get("phone_3") or "+52",
            data.get("phone_4") or "+52",
            data.get("phone_5") or "+52"
        ]
        
        type_phones = [
            'INVALID',
            'INVALID',
            'INVALID',
            'INVALID',
            'INVALID'
        ]
        
        i = 0
        
        for phone in phones:
            
            if not phone:
                continue
            
            if len(phone) < 10:
                continue

            phone = phone.replace("+", "")
            phone = phone.replace("(", "")
            phone = phone.replace(")", "")
            phone = phone.replace("-", "")
            phone = phone.replace(".", "")
            phone = phone.replace(" ", "")

            if len(phone) < 10:
                continue

            if len(phone) > 10:
                phone = phone[len(phone)-10:]

            nir = phone[:3]
            serie = phone[3:6]
            number = int(phone[6:])

            print(f"{nir} - {serie} - {number}")

            pnn_list = Pnn.objects.filter(nir=nir, serie=serie)

            tipo_red = "INVALID"

            for pnn in pnn_list:
                if number >= pnn.num_inicial and number <= pnn.num_final:
                    pnn = pnn

                    if not pnn:
                        continue

                    tipo_red = pnn.tipo_red

            type_phones[i] = tipo_red
            
            phones[i] = f"+52 {phone}"
            i = i + 1
        
        data['phone_1'] = phones[0]
        data['phone_2'] = phones[1]
        data['phone_3'] = phones[2]
        data['phone_4'] = phones[3]
        data['phone_5'] = phones[4]
        
        data['type_phone_1'] = type_phones[0]
        data['type_phone_2'] = type_phones[1]
        data['type_phone_3'] = type_phones[2]
        data['type_phone_4'] = type_phones[3]
        data['type_phone_5'] = type_phones[4]
        
        validated_customers.append(data)
        
        data_file = {
            "total": len(validated_customers),
            "customers": validated_customers
        }

        data = json.dumps(data_file, indent=4)

        # Writing to sample.json
        with open("validated.json", "w") as outfile:
            outfile.write(data)
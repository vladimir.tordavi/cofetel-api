from rest_framework.views import APIView
from rest_framework.response import Response
from core.api_mixins import DefaultMixins
from django.db import IntegrityError
from pnn.models import Pnn
from pnn.serializers import PnnSerializer
from pnn.validate import validate
from pnn.pnn_data import load_data
import json


# Create your views here.
class PnnView(APIView, DefaultMixins):

    def get(self, request, **kwargs):

        try:
            id=kwargs.get('id')
            pnn = Pnn.objects.filter(pk=id).first()
            pnnSerializer = PnnSerializer(pnn)

            if not pnn:
                return Response({
                    'detail': f'No se ha encontrado el pnn con ID {id}',
                }, status=404)

            return Response({
                'message': 'API VIEW GET METHOD',
                'pnn': pnnSerializer.data
            }, status=200)

        except ValueError as e:
            return Response({
                'detail': 'ERROR ON GET PNN',
                'pnn': pnnSerializer.data
            }, status=500)

    def create(self, pnn_data):

        created = False

        try:
            data = pnn_data

            clave_censal = data.get("CLAVE_CENSAL") or data.get("clave_censal")
            poblacion = data.get("POBLACION") or data.get("poblacion")
            municipio = data.get("MUNICIPIO") or data.get("municipio")
            estado = data.get("ESTADO") or data.get("estado")
            presuscripcion = data.get("PRESUSCRIPCION") or data.get("presuscripcion")
            region = data.get("REGION") or data.get("region")
            asl = data.get("ASL") or data.get("asl")
            nir = data.get("NIR") or data.get("nir")
            serie = data.get("SERIE") or data.get("serie")
            num_inicial = data.get("NUMERACION_INICIAL") or data.get("num_inicial")
            num_final = data.get("NUMERACION_FINAL") or data.get("num_final")
            ocupacion = data.get("OCUPACION") or data.get("ocupacion")
            tipo_red = data.get("TIPO_RED") or data.get("tipo_red")
            modalidad = data.get("MODALIDAD") or data.get("modalidad")
            razon_social = data.get("RAZON_SOCIAL") or data.get("razon_social")
            fecha_asignacion = data.get("FECHA_ASIGNACION") or data.get("fecha_asignacion")
            fecha_consolidacion = data.get("FECHA_CONSOLIDACION") or data.get("fecha_consolidacion")
            fecha_migracion = data.get("FECHA_MIGRACION") or data.get("fecha_migracion")
            nir_anterior = data.get("NIR_ANTERIOR") or data.get("nir_anterior")


            print(f"NIR: {nir}")

            if not nir:
                print("Body field NIR is required")
                return created

            if not serie:
                print("Body field SERIE is required")
                return created

            if not num_inicial:
                print("Body field NUMERACION_INICIAL is required")
                return created

            if not num_final:
                print("Body field NUMERACION_FINAL is required")
                return created

            nir = str(nir)
            num_inicial = int(num_inicial)
            num_final = int(num_final)

            pnnSerializer = PnnSerializer(
                data = {
                    clave_censal:clave_censal,
                    poblacion:poblacion,
                    municipio:municipio,
                    estado:estado,
                    presuscripcion:presuscripcion,
                    region:region,
                    asl:asl,
                    nir:nir,
                    serie:serie,
                    num_inicial:num_inicial,
                    num_final:num_final,
                    ocupacion:ocupacion,
                    tipo_red:tipo_red,
                    modalidad:modalidad,
                    razon_social:razon_social,
                    fecha_asignacion:fecha_asignacion,
                    fecha_consolidacion:fecha_consolidacion,
                    fecha_migracion:fecha_migracion,
                    nir_anterior:nir_anterior
                }
            )

            if not pnnSerializer.is_valid():

                print(f"ERROR AT SERIALIZE: {pnnSerializer.errors}")
                return created

            pnn = Pnn.objects.create(
                clave_censal=clave_censal,
                poblacion=poblacion,
                municipio=municipio,
                estado=estado,
                presuscripcion=presuscripcion,
                region=region,
                asl=asl,
                nir=nir,
                serie=serie,
                num_inicial=num_inicial,
                num_final=num_final,
                ocupacion=ocupacion,
                tipo_red=tipo_red,
                modalidad=modalidad,
                razon_social=razon_social,
                fecha_asignacion=fecha_asignacion,
                fecha_consolidacion=fecha_consolidacion,
                fecha_migracion=fecha_migracion,
                nir_anterior=nir_anterior
            )

            if not pnn:

                print(f"ERROR AT CREATE: {clave_censal}")
                return created

            created = True

        except TypeError as e:

            print(f"DATO INCORRRECTO: {e.args[0]}")
            return created

        except IntegrityError as e:

            print(f"DATO INVALIDO: {e.args[0]}")
            return created

        except ValueError as e:

            print(f"ERROR AL CREAR: {e.args[0]}")
            return created

        return created

    def post(self, request, **kwargs):
        try:
            data = json.loads(request.body)
            pnn_list = data.get("pnn_list")

            if not pnn_list:
                return Response({
                    'detail': 'Body field pnn_list is required'
                }, status=406)

            for pnn in pnn_list:
                self.create(pnn)

        except ValueError as e:

            print(f"ERROR {e.args[0]}")
            return Response({
                'message': 'Error al crear pnn',
                'detail': e.args[0]
            }, status=500)
    
    def put(self, request, **kwargs):

        try:
            data = request.data.copy()
            clave_censal = data.get("CLAVE_CENSAL") or data.get("clave_censal")
            poblacion = data.get("POBLACION") or data.get("poblacion")
            municipio = data.get("MUNICIPIO") or data.get("municipio")
            estado = data.get("ESTADO") or data.get("estado")
            presuscripcion = data.get("PRESUSCRIPCION") or data.get("presuscripcion")
            region = data.get("REGION") or data.get("region")
            asl = data.get("ASL") or data.get("asl")
            nir = data.get("NIR") or data.get("nir")
            serie = data.get("SERIE") or data.get("serie")
            num_inicial = data.get("NUMERACION_INICIAL") or data.get("num_inicial")
            num_final = data.get("NUMERACION_FINAL") or data.get("num_final")
            ocupacion = data.get("OCUPACION") or data.get("ocupacion")
            tipo_red = data.get("TIPO_RED") or data.get("tipo_red")
            modalidad = data.get("MODALIDAD") or data.get("modalidad")
            razon_social = data.get("RAZON_SOCIAL") or data.get("razon_social")
            fecha_asignacion = data.get("FECHA_ASIGNACION") or data.get("fecha_asignacion")
            fecha_consolidacion = data.get("FECHA_CONSOLIDACION") or data.get("fecha_consolidacion")
            fecha_migracion = data.get("FECHA_MIGRACION") or data.get("fecha_migracion")
            nir_anterior = data.get("NIR_ANTERIOR") or data.get("nir_anterior")

            pnn: Pnn = Pnn.objects.filter(pk=id).first()

            if not pnn:
                return Response({
                    'detail': f'Pnn with ID {id} not found',
                }, status=404)

            pnn.clave_censal=clave_censal,
            pnn.poblacion=poblacion,
            pnn.municipio=municipio,
            pnn.estado=estado,
            pnn.presuscripcion=presuscripcion,
            pnn.region=region,
            pnn.asl=asl,
            pnn.nir=nir,
            pnn.serie=serie,
            pnn.num_inicial=num_inicial,
            pnn.num_final=num_final,
            pnn.ocupacion=ocupacion,
            pnn.tipo_red=tipo_red,
            pnn.modalidad=modalidad,
            pnn.razon_social=razon_social,
            pnn.fecha_asignacion=fecha_asignacion,
            pnn.fecha_consolidacion=fecha_consolidacion,
            pnn.fecha_migracion=fecha_migracion,
            pnn.nir_anterior=nir_anterior

            pnn.save()

            if not pnn:
                return Response({
                    'detail': 'PNN CANT BE UPDATED',
                }, status=406)

            return Response({
                'message': 'PNN UPDATED SUCCESFULLY',
            }, status=201)

        except TypeError as e:
            return Response({
                'message': 'Dato incorrecto',
                'detail': e.args[0]
            }, status=500)

        except IntegrityError as e:
            return Response({
                'message': 'Datos invalidos',
                'detail': e.args[0]
            }, status=500)
            
        except ValueError as e:

            return Response({
                'message': 'Error al crear pnn',
                'detail': e.args[0]
            }, status=500)

    def delete(self, request, **kwargs):
        
        try:

            id = kwargs.get('id')
            pnn:Pnn = Pnn.objects.filter(pk=id).first()
            print(pnn)
            if not pnn:
                return Response({
                    'detail': f'No se ha encontrado gente con ID: {id}',
                }, status=200)

            pnn.delete()

            return Response({
                'message': f'Pnne con ID {id} eliminado con éxito',
            }, status=200)

        except ValueError as e:

            return Response({
                'message': 'Error al eliminar pnn',
                'detail': e.args[0]
            }, status=500)


class PhoneView(APIView):

    def get(self, request, **kwargs):
        
        phone = kwargs.get("phone")
        print(phone)

        if not phone:
            return Response({
                'detail': 'Path field phone is required'
            }, status=400)

        phone = phone.replace("+", "")
        phone = phone.replace("(", "")
        phone = phone.replace(")", "")
        phone = phone.replace("-", "")
        phone = phone.replace(".", "")
        phone = phone.replace(" ", "")

        if len(phone) < 10:
            return Response({
                'detail': 'Phone is invalid'
            }, status=400)

        if len(phone) > 10:
            phone = phone[len(phone)-10:]

        print(f"phone: {phone}")

        nir = phone[:3]
        serie = phone[3:6]
        number = int(phone[6:])

        print(f"{nir} - {serie} - {number}")

        pnn_list = Pnn.objects.filter(nir=nir, serie=serie)


        for pnn in pnn_list:
            if number >= pnn.num_inicial and number <= pnn.num_final:
                pnn_serializer = PnnSerializer(pnn)
                
        if not pnn_serializer:
            
            return Response({
                'detail': 'Phone is invalid'
            }, status=400)

        print(f"PNN_LIST: {len(pnn_list)}")

        return Response({
            'message': f'Phone validated',
            'phone': phone,
            'pnn': pnn_serializer.data
        }, status=200)

    def post(self, request, **kwargs):
        
        print("VALIDATING DATA")
        try:
            validate()
            return Response({
                'message': f'Phones validated',
            }, status=200)
        except ValueError as e:
            return Response({
                'message': 'Error al validar teléfonos',
                'detail': e.args[0]
                
            }, status=500)

class DataView(APIView):

    def post(self, request, **kwargs):

        try:
            load_data()
            return Response({
                'message': f'Phones validated',
            }, status=200)

        except ValueError as e:
            return Response({
                'message': 'Error al validar teléfonos',
                'detail': e.args[0]
                
            }, status=500)
        
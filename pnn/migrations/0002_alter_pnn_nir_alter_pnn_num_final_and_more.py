# Generated by Django 4.0.3 on 2022-11-24 22:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pnn', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pnn',
            name='nir',
            field=models.CharField(blank=True, max_length=180),
        ),
        migrations.AlterField(
            model_name='pnn',
            name='num_final',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='pnn',
            name='num_inicial',
            field=models.IntegerField(blank=True),
        ),
        migrations.AlterField(
            model_name='pnn',
            name='serie',
            field=models.CharField(blank=True, max_length=180),
        ),
    ]

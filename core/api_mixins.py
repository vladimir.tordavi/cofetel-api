from rest_framework import authentication
from rest_framework.permissions import IsAuthenticated


class DefaultMixins(object):

    authentication_clases = (
        authentication.BasicAuthentication,
        authentication.TokenAuthentication
    )

    permission_classes = (IsAuthenticated)
from django.db import models

"""
    An abstract base class model that provides self-updating ``created`` and ``updated_at`` fields.
"""
class TimeStampedModel(models.Model):

    created = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True
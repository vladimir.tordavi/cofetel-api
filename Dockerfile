FROM python:3.9-alpine

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN mkdir /code

WORKDIR /code

COPY . ./

RUN python -m pip install --upgrade pip

RUN pip install -r requirements/requirements.txt

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

COPY . /code

RUN ls -a